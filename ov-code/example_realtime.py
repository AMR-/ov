from occuviz.realtime_visualization import RealtimeVisualization
from occuviz.visualization import StateConfiguration

policy = TestPolicy()
state_config = StateConfiguration()
env = JackalEnvOrTestEnv()

rv = RealtimeVisualization(policy, state_config, env)
rv.run()
