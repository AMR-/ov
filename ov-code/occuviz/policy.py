from abc import ABCMeta, abstractmethod


class Policy(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_action(self, state):
        pass
    
    # TODO if you want to enforce that a policy has other attributes, you can
