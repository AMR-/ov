import math
from .policy import Policy


class StateConfiguration(object):

    def __init__(self,
                 num_columns=10,  # handle positive integers
                 row_heights=(0.2, 0.2, 0.2, 0.3, 1, 1, 1),
                 starting_distance=0.1,
                 left_radian_bounds=-(2. / 3.) * math.pi,  # handle from -pi to 0
                 right_radian_bounds=(2. / 3.) * math.pi,  # handle from 0 to pi
                 # TODO, optionally, handle other kinds of states
                 state_length=213,
                 goal_state_indices=(0, 1),
                 grid_indices=((3, 73), (73, 143), (143, 213)),
                 static_defaults=frozenset({2: 0}.items()),
                 ):
        # TODO -- handle different configurations
        pass


class Visualization(object):

    def __init__(self, policy: Policy, state_configuration: StateConfiguration):
        # TODO
        pass

