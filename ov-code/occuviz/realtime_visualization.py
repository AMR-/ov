from .visualization import Visualization, StateConfiguration
from .policy import Policy
import gym


class RealtimeVisualization(Visualization):
    def __init__(self, policy: Policy, state_configuration: StateConfiguration, env: gym.Env):
        super().__init__(policy, state_configuration)
        self.env = env
        # TODO
        pass

    def run(self):
        # TODO
        pass
