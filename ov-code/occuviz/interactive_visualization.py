from .visualization import Visualization, StateConfiguration
from .policy import Policy


class InteractiveVisualization(Visualization):
    def __init__(self, policy: Policy, state_configuration: StateConfiguration):
        super().__init__(policy, state_configuration)
        # TODO
        pass

    def run(self):
        # TODO
        pass
