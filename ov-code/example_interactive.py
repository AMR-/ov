from occuviz.interactive_visualization import InteractiveVisualization
from occuviz.visualization import StateConfiguration

policy = TestPolicy()
state_config = StateConfiguration()

iv = InteractiveVisualization(policy, state_config)
iv.run()
